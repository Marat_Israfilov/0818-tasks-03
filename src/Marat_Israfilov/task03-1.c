/*Программа, выводящая строки на экран в обратном порядке*/
#include <stdio.h>
#include <string.h>
#define N 5
#define M 128

int input_strings(char str[N][M]) //Вводим N строк или до пустой строки и возвращаем кол-во введеных строк
{
	int i, count;
	count = 0;

	for(i = 0; i < N; i++)
	{	
		fgets(str[i], M, stdin);
		str[i][strlen(str[i])-1] = '\0';

		if(strlen(str[i]) == 0)
		{
			break;
		}

		count++;
	}

	printf("\n");
	return count;
}

void output_strings(char str[N][M], int count) //Выводим строки в обратном порядке
{
	int i;
	
	for(i = count; i >= 0 ; i--)
	{
		printf("%s\n", str[i]);		
	}
}

int main()
{
	char str[N][M];
	int num;
	num = input_strings(str);
	output_strings(str, num);
	return 0;
}
