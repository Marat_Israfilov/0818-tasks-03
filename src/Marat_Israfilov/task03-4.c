/*Программа, выводящая строки в случайном порядке*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 5
#define M 128

int input_strings(char str[N][M]) //Вводим N строк или до пустой строки и возвращаем кол-во введеных строк
{
        int i, count;
        count = 0;

        for(i = 0; i < N; i++)
        {
                fgets(str[i], M, stdin);
                str[i][strlen(str[i])-1] = '\0';

                if(strlen(str[i]) == 0)
                {
                        break;
                }

                count++;
        }

        printf("\n");
        return count;
}

void index_sort(int count, int index[N]) //"сортируем" индексы строк в случайном порядке
{
        int i, tmp, rnd;
	srand(time(0));

        for(i = 0; i < count; i++)
        {
                index[i] = i;
        }

        for(i = 0; i < count; i++)
        {
		rnd = rand() % count;
		tmp = index[i];
		index[i] = index[rnd];
		index[rnd] = tmp;
	}
}

void output_strings(char str[N][M], int count, int index[N]) //Выводим строки
{
        int i;

        for(i = 0; i < count ; i++)
        {
                printf("%s\n", str[index[i]]);
        }
}

int main()
{
        char str[N][M];
        int i, num, index[N];

        num = input_strings(str);
        index_sort(num, index);
        output_strings(str, num, index);

        return 0;
}
