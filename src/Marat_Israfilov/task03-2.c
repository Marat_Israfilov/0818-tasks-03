/*Программа, выводящая строку со случайным номером*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 5
#define M 128

int input_strings(char str[N][M]) //Вводим N строк или до пустой строки и возвращаем кол-во введеных строк
{
        int i, count;
        count = 0;

        for(i = 0; i < N; i++)
        {
                fgets(str[i], M, stdin);
                str[i][strlen(str[i])-1] = '\0';

                if(strlen(str[i]) == 0)
                {
                        break;
                }

                count++;
        }

        printf("\n");
        return count;
}

void output_strings(char str[N][M], int count) //Выводим случайную строку
{
        int i, tmp;
	srand(time(0));
        
	tmp = rand() % count;
        printf("%s\n", str[tmp]);        
}

int main()
{
        char str[N][M];
        int num;
        num = input_strings(str);
        output_strings(str, num);
        return 0;
}
