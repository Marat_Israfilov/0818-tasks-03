/*Программа, выводящая строки в порядке возрастания их длин*/
#include <stdio.h>
#include <string.h>
#define N 5
#define M 128

int input_strings(char str[N][M]) //Вводим N строк или до пустой строки и возвращаем кол-во введеных строк
{
        int i, count;
        count = 0;

        for(i = 0; i < N; i++)
        {
                fgets(str[i], M, stdin);
                str[i][strlen(str[i])-1] = '\0';

                if(strlen(str[i]) == 0)
                {
                        break;
                }

                count++;
        }

        printf("\n");
        return count;
}

void str_sort(char str[N][M], int count, int index[N])
{
	int i, j, tmp;
	
	for(i = 0; i < count; i++)
	{
		index[i] = i;
	}
	
	for(i = 0; i < count-1; i++)
	{
		for(j = 0; j < count-i-1; j++)
		{
			if(strlen(str[index[j]]) > strlen(str[index[j+1]]))
			{
				tmp = index[j];
				index[j] = index[j+1];
				index[j+1] = tmp;
			}
		}
	}
}

void output_strings(char str[N][M], int count, int index[N]) //Выводим строки
{
        int i;

        for(i = 0; i < count ; i++)
        {
                printf("%s\n", str[index[i]]);
        }
}

int main()
{
        char str[N][M];
        int num, index[N];
	
        num = input_strings(str);
	str_sort(str, num, index);	
        output_strings(str, num, index);

        return 0;
}
